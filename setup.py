#!/usr/bin/env python3
import os, glob, sys
import os.path
from shutil import copyfile
from setuptools import find_namespace_packages
from setuptools import setup

# handle flags for package manager like aurman and pacaur.
# Allow both environment variable and command line flag
forceSettingsFlag = (
    "--force-settings" in sys.argv or
    os.environ.get('FENRIR_FORCE_SETTINGS') == '1'
)
if "--force-settings" in sys.argv:
    sys.argv.remove("--force-settings")

dataFiles = []

# Handle locale files
localeFiles = glob.glob('locale/*/LC_MESSAGES/*.mo')
for localeFile in localeFiles:
    lang = localeFile.split(os.sep)[1]
    destDir = f'/usr/share/locale/{lang}/LC_MESSAGES'
    dataFiles.append((destDir, [localeFile]))

# Handle other configuration files
directories = glob.glob('config/*')
for directory in directories:
    files = glob.glob(directory+'/*')
    destDir = ''
    if 'config/punctuation' in directory :
        destDir = '/etc/fenrirscreenreader/punctuation'
    elif 'config/keyboard' in directory:
        destDir = '/etc/fenrirscreenreader/keyboard'
    elif 'config/settings' in directory:
        destDir = '/etc/fenrirscreenreader/settings'
        if not forceSettingsFlag:
            try:
                files = [f for f in files if not f.endswith('settings.conf')]
            except:
                pass
    elif 'config/scripts' in directory:
        destDir = '/usr/share/fenrirscreenreader/scripts'
    if destDir != '':
        dataFiles.append((destDir, files))

files = glob.glob('config/sound/default/*')
destDir = '/usr/share/sounds/fenrirscreenreader/default'
dataFiles.append((destDir, files))
files = glob.glob('config/sound//template/*')
destDir = '/usr/share/sounds/fenrirscreenreader/template'
dataFiles.append((destDir, files))
files = glob.glob('tools/*') 
dataFiles.append(('/usr/share/fenrirscreenreader/tools', files))
dataFiles.append(('/usr/share/man/man1', ['docs/fenrir.1']))

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    # Application name:
    name="fenrir-screenreader",
    # description
    description="A TTY Screen Reader for Linux.",
    long_description=read('README.md'),
    long_description_content_type="text/markdown",
    keywords=['screenreader', 'a11y', 'accessibility', 'terminal', 'TTY', 'console'],
    license="License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
    url="https://git.stormux.org/storm/fenrir/",
    classifiers=[
        "Programming Language :: Python",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Development Status :: 5 - Production/Stable",
        "Topic :: Multimedia :: Sound/Audio :: Speech",
        "Environment :: Console",
    ],

    # Application author details:
    author="Storm Dragon, Jeremiah, Chrys and others",
    author_email="storm_dragon@stormux.org",

    # Packages
    package_dir={'': 'src'},
    packages=find_namespace_packages(
        where='src',
        include=['fenrirscreenreader*']
    ),
    scripts=['src/fenrir'],

    # Include additional files into the package
    include_package_data=True,
    zip_safe=False,

    data_files=dataFiles,

    # Dependent packages (distributions)
    python_requires='>=3.6',
    install_requires=[
        "evdev>=1.1.2",
        "daemonize>=2.5.0",
        "dbus-python>=1.2.8",
        "pyudev>=0.21.0",
        "setuptools",
        "pexpect",
        "pyttsx3",
        "pyte>=0.7.0",
    ],
)

if not forceSettingsFlag:
    print('')
    # create settings file from example if not exist
    if not os.path.isfile('/etc/fenrirscreenreader/settings/settings.conf'):
        try:
            copyfile('/etc/fenrirscreenreader/settings/settings.conf.example', '/etc/fenrirscreenreader/settings/settings.conf')
            print('create settings file in /etc/fenrirscreenreader/settings/settings.conf')
        except:
            pass
    else:
        print('settings.conf file found. It is not overwritten automatical')

print('')
print('To have Fenrir start at boot:')
print('sudo systemctl enable fenrir')
print('Pulseaudio users may want to run:')
print('/usr/share/fenrirscreenreader/tools/configure_pulse.sh')
print('once as their user account and once as root to configure Pulseaudio.')
print('Please install the following packages manually:')
print('- Speech-dispatcher: for the default speech driver')
print('- Espeak: as basic TTS engine')
print('- sox: is a player for the generic sound driver')
