#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Fenrir TTY screen reader
# By Chrys, Storm Dragon, and contributers.

from fenrirscreenreader.core import debug
from enum import Enum

class fenrirEventType(Enum):
    Ignore = 0
    StopMainLoop = 1
    ScreenUpdate = 2
    KeyboardInput = 3
    PlugInputDevice = 4
    ScreenChanged = 5
    HeartBeat = 6
    ExecuteCommand = 7
    ByteInput = 8
    RemoteIncomming = 9
    def __int__(self):
        return self.value
    def __str__(self):
        return self.name
