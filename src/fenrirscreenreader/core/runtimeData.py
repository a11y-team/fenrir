#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Fenrir TTY screen reader
# By Chrys, Storm Dragon, and contributers.

from fenrirscreenreader.core import debug

runtimeData = {
'speechDriver': None,
'screenDriver': None,
'soundDriver': None,
'inputDriver': None,
'remoteDriver': None,
'inputManager': None,
'commandManager': None,
'screenManager': None,
'outputManager': None,
'debug':None,
}
