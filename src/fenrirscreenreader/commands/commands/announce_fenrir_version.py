#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Fenrir TTY screen reader
# By Chrys, Storm Dragon, and contributers.

from fenrirscreenreader.core import debug
from fenrirscreenreader import fenrirVersion

class command():
    def __init__(self):
        pass
    def initialize(self, environment):
        self.env = environment
    def shutdown(self):
        pass
    def getDescription(self):
        return _('Present the version of Fenrir currrrently in use.')
    
    def run(self):
        try:
            self.env['runtime']['outputManager'].presentText(f'Fenrir screen reader version {fenrirVersion.version}-{fenrirVersion.codeName}', interrupt=True)
        except exception as e:
            self.env['runtime']['outputManager'].presentText(_('Version information is unavailable.'), interrupt=True)
            
    def setCallback(self, callback):
        pass
